<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

define('INTRANET_FIELD_CATEGORIES', 'field_categories');
define('INTRANET_FIELD_CONTACT_INFORMATION', 'field_contact_information');
define('INTRANET_FIELD_EXPIRATION_DATE', 'field_expiration_date');
define('INTRANET_FIELD_FACEBOOK_PAGE', 'field_facebook_page');
define('INTRANET_FIELD_LOGO', 'field_logo');
define('INTRANET_FIELD_PUBLISHING_DATE', 'field_publishing_date');
define('INTRANET_FIELD_STAFF_LANGUAGES', 'field_staff_languages');
define('INTRANET_FIELD_TIMES', 'field_times');
define('INTRANET_FIELD_TWITTER_HANDLE', 'field_twitter_handle');

define('INTRANET_ROLE_CONTENT_MANAGER', 'content manager');
define('INTRANET_ROLE_ADMINISTRATOR', 'administrator');

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function intranet_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}
